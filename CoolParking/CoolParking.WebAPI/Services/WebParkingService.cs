﻿using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class WebParkingService : IWebParkingService
    {
        private IParkingService parkingService;
        public WebParkingService(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }
        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
