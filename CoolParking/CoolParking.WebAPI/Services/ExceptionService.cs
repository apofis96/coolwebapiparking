﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Services
{
    public static class ExceptionService
    {
        public static Dictionary<string, string> ConvertToDictionary(Exception e)
        {
            Dictionary<string, string> error = new Dictionary<string, string>
            {
                { "exception", e.GetType().ToString() },
                { "message", e.Message}
            };
            return error;
        }
        public static Dictionary<string, string> ConvertToDictionary(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary modelstate)
        {
    
            Dictionary<string, string> error = new Dictionary<string, string>
            {
                { "exception", "Validation error" },
                { "message", modelstate.Values.Last().Errors.Last().ErrorMessage}
            };
            return error;
        }
    }
}