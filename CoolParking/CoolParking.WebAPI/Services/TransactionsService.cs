﻿using System;
using System.Collections.Generic;
using CoolParking.WebAPI.Models;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService : ITransactionsService
    {
        private IParkingService parkingService;
        public TransactionsService(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public List<Transaction> GetLast()
        {
            List<Transaction> transactions = new List<Transaction>();
            foreach (TransactionInfo transactionInfo in parkingService.GetLastParkingTransactions())
            {
                transactions.Add(new Transaction(transactionInfo.VehicalId, transactionInfo.Sum, transactionInfo.Date));
            }
            return transactions;
        }
        public string GetAll()
        {
            return parkingService.ReadFromLog();
        }
        public WebVehicle TopUp(TopUpVehicle vehicle)
        {
            if (!Vehicle.ValidateRegistrationPlateNumber(vehicle.VehicleId))
            {
                throw new ArgumentException("Wrong ID format");
            }
            try
            {
                parkingService.TopUpVehicle(vehicle.VehicleId, vehicle.Sum.GetValueOrDefault());
            }
            catch (ArgumentException e)
            {
                if (e.Message == "Vehicle with this Id not parked")
                {
                    throw new InvalidOperationException(e.Message);
                }
                throw;
            }
            return new VehiclesService(parkingService).Get(vehicle.VehicleId);
        }
    }
}
