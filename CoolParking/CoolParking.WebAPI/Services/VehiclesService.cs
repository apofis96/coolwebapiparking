﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.WebAPI.Models;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesService : IVehiclesService
    {
        private IParkingService parkingService;
        public VehiclesService(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        public List<WebVehicle> GetAll()
        {
            List<WebVehicle> vehicles = new List<WebVehicle>();
            foreach (var vh in parkingService.GetVehicles())
            {
                vehicles.Add(new WebVehicle { Id = vh.Id, Balance = vh.Balance, VehicleType = (int)vh.VehicleType });
            }
            return vehicles;
        }
        public WebVehicle Get(string id)
        {
            if (!BL.Models.Vehicle.ValidateRegistrationPlateNumber(id))
            {
                throw new ArgumentException("Wrong ID format");
            }
            var selectedVehicles = from vehicle in GetAll()
                                   where vehicle.Id == id
                                   select vehicle;
            if (selectedVehicles.Count() == 0)
            {
                throw new InvalidOperationException("Vehicle not found");
            }
            return selectedVehicles.First();
        }
        public WebVehicle Post(WebVehicle vehicle)
        {
            parkingService.AddVehicle(new BL.Models.Vehicle(vehicle.Id, (BL.Models.VehicleType)vehicle.VehicleType, vehicle.Balance.GetValueOrDefault()));
            return vehicle;
        }
        public void Delete(string id)
        {
            if (!BL.Models.Vehicle.ValidateRegistrationPlateNumber(id))
            {
                throw new InvalidOperationException("Wrong ID format");
            }
            parkingService.RemoveVehicle(id);
        }
    }
}
