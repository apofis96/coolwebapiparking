using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using System.IO;
using System.Reflection;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().ConfigureApiBehaviorOptions(options => options.SuppressModelStateInvalidFilter = true);
            services.AddSingleton<IParkingService>(sp => new ParkingService(
                new TimerService(Settings.chargeoffPeriod), 
                new TimerService(Settings.loggingPeriod), 
                new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log")));
            services.AddTransient<IVehiclesService, VehiclesService>();
            services.AddTransient<ITransactionsService, TransactionsService>();
            services.AddTransient<IWebParkingService, WebParkingService>();
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
