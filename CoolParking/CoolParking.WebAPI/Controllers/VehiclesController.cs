﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IVehiclesService vehiclesService;
        public VehiclesController(IVehiclesService vehiclesService)
        {
            this.vehiclesService = vehiclesService;
        }
        [HttpGet]
        public ActionResult<List<WebVehicle>> GetAll()
        {
            return Ok(vehiclesService.GetAll());
        }
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<WebVehicle> Get(string id)
        {
            try
            {
                return Ok(vehiclesService.Get(id));
            }
            catch (ArgumentException e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
            catch (InvalidOperationException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
        [HttpPost]
        public ActionResult<WebVehicle> Post([FromBody] WebVehicle vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(ModelState));
            }
            try
            {
                WebVehicle vh = vehiclesService.Post(vehicle);
                return CreatedAtRoute("Get", new { vh.Id }, vh);
            }
            catch (Exception e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                vehiclesService.Delete(id);
                return NoContent();
            }
            catch(InvalidOperationException e)
            {
                return BadRequest(Services.ExceptionService.ConvertToDictionary(e));
            }
            catch (ArgumentException e)
            {
                return NotFound(Services.ExceptionService.ConvertToDictionary(e));
            }
        }
    }
}