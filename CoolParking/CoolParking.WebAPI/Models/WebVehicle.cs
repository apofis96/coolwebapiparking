﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Models
{
    public class WebVehicle
    {
        [JsonPropertyName("id")]
        [Required]
        public string Id { get; set; }
        [JsonPropertyName("vehicleType")]
        [Required]
        [Range(0, 4)]
        public int? VehicleType { get; set; }
        [JsonPropertyName("balance")]
        [Required]
        public decimal? Balance { get; set; }

    }
}
