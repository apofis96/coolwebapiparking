﻿namespace CoolParking.WebAPI.Interfaces
{
    public interface IWebParkingService
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
    }
}
