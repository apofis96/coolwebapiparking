﻿using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsService
    {
        List<Transaction> GetLast();
        string GetAll();
        WebVehicle TopUp(TopUpVehicle vehicle);
    }
}
