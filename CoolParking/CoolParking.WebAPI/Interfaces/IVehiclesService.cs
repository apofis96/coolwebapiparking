﻿using System.Collections.Generic;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesService
    {
        List<WebVehicle> GetAll();
        WebVehicle Get(string id);
        WebVehicle Post(WebVehicle vehicle);
        void Delete(string id);
    }
}
