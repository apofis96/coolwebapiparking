﻿using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : CoolParking.BL.Interfaces.ILogService
    {
        public string LogPath { get; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }
        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = new StreamWriter(LogPath, true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
        public string Read()
        {
            string log = "";
            if (File.Exists(LogPath))
            {
                using (StreamReader streamReader = new StreamReader(LogPath))
                {
                    log = streamReader.ReadToEnd();
                }
            }
            else
            {
                throw new InvalidOperationException("Logfile not found at "+ LogPath);
            }
            return log;
        }
    }
}