﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal parkingInitialBalance = 0M;
        public static int parkingCapacity = 10;
        public static int chargeoffPeriod = 5;
        public static int loggingPeriod = 60;
        public static Dictionary<VehicleType, decimal> tariffs = new Dictionary<VehicleType, decimal>
        {
            [VehicleType.PassengerCar] = 2M,
            [VehicleType.Truck] = 5M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Motorcycle] = 1M
        };
        public static decimal penaltyRatio = 2.5M;
    }
}